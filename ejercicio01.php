<!DOCTYPE html>
<html>
<head>
	<title>Suma y resta de dos números enteros</title>
	<script>
		function calcular() {

			var a = parseInt(document.getElementById("numero1").value);
			var b = parseInt(document.getElementById("numero2").value);
			

			var suma = a + b;
			var resta = a - b;
			

			document.getElementById("resultado-suma").innerHTML = "La suma de " + a + " y " + b + " es: " + suma;
			document.getElementById("resultado-resta").innerHTML = "La resta de " + a + " y " + b + " es: " + resta;
		}
	</script>
</head>
<body>
<body bgcolor="green">
	<h1>Suma y resta de dos números enteros</h1>
	
	<label for="numero1">Número 1:</label>
	<input type="number" id="numero1">
	
	<label for="numero2">Número 2:</label>
	<input type="number" id="numero2">
	
	<button onclick="calcular()">Calcular</button>
	
	<p id="resultado-suma"></p>
	<p id="resultado-resta"></p>
</body>
</html>